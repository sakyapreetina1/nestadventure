
jQuery(document).ready(function($){
    SliderBlock1();
    TestimonialSLider();
    if(window.innerWidth < 770) {
        LogoSLider();
    }
    //Slider Block 1 
    function SliderBlock1(){
    jQuery('.slider-block-1').slick({
    slidesToShow : 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    centerMode: false,
    autoplay:false,
    autoplaySpeed:3000,
    infinite:true,
    prevArrow: $(".arrow-left"),
    nextArrow: $(".arrow-right"),
    responsive: [
      
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
      

  ]
    });
        }
//Testimonial Slider
        function TestimonialSLider(){
            jQuery('.testimonial-slider').slick({
            slidesToShow : 1,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            centerMode: false,
            autoplay:false,
            autoplaySpeed:3000,
            infinite:true,
            prevArrow: $(".testimonial-arrow-left"),
            nextArrow: $(".testimonial-arrow-right"),
            responsive: [
              
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
        
          ]
            });
                }
     //Logo Slider
     function LogoSLider(){
        jQuery('.logos ul').slick({
        slidesToShow : 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        centerMode: false,
        autoplay:true,
        autoplaySpeed:3000,
        infinite:true,
        prevArrow:'<div class="arrow-left"><img src="images/left-arrow.png" alt="left-arrow"></div>',
        nextArrow:'<div class="arrow-right"><img src="images/right-arrow.png" alt="right-arrow"></div>',
       
        });
            }           
   });

//Burger Menu
jQuery(document).ready(function(){
    jQuery('#menu-icon').on('click', function(){
        jQuery('.main-nav').toggleClass('expand');
        jQuery('body').toggleClass('hide-scroll');
      return false;
    });
  });